# The organization of OpenCraft

## Context

Small companies can be very efficient, and allow for organization to be relatively informal. Because everyone knows each other well, it's easier than in larger structure to work together. A small company with self-driven contributors who care about producing quality results need very little oversight. It's easy for the leadership to communicate what needs to be done, as well as how and why - it's a small group.

When small companies grow however, everyone knows each other less well, and the assumption that everyone can be on the same page breaks down. It's true of individual contributors, who know less about each other's work, get less chances to work with all their colleagues as closely, and are thus more likely to step on each other's toes - or at least be less able to help each other. It's also true of management, which get its time divided between more reports and projects, and are thus less knowledgeable about each of the decision they have to take, have more difficulty communicating goals to everyone, and are also less able to help each report individually.

Most companies solve this issue by adding middle managers, who each manage a subset of the company, a team, and in turn report to the top management. It preserves some of the advantages of small companies: members of a team can know each other better again, and the team's manager has more time for each member. It seems familiar, but it's actually really different, because the middle manager has less capacity to take decisions than the top management team, and yet it is still responsible for solving the same issues. For a lot of problems - often the most important ones - decisions will involve the top management. Effectively, middle management exacerbate the distance between individual contributors and the decision made, adding extra layers of filters with each management layer.

On the other hand, when companies try to keep growing without middle managers, most have failed. The rare successes show that self-management can work, even at scale: Buurtzorg grew to 9,000 nurses, AES to 40,000 employees (see [Reinventing Organizations](https://www.amazon.com/Reinventing-Organizations-Frederic-Laloux/dp/2960133501/), by Frederic Laloux). But the number of companies who have tried and failed or ended up preferring middle management is a word of caution: Google [[1](https://www.businessinsider.com/larry-page-the-untold-story-2014-4)], Medium [[2](https://blog.medium.com/management-and-organization-at-medium-2228cc9d93e9)], Buffer [[3](https://open.buffer.com/self-management-hierarchy/)], etc.

At OpenCraft, we take an intermediary approach: use self-management in a targeted way, for specific responsibilities, while preserving a minimal hierarchy to address critical situations and act as safeguard. The goal with this approach is to distribute enough decision-making throughout the company to avoid the need for middle managers, while still retaining the capability to take some decisions in a centralized way when it is useful. An important goal is to make sure the final decision-maker is directly involved, and not isolated in an ivory tower behind intermediaries.

Note that we use some aspects of self-management, but OpenCraft still has some key aspects of hierarchical organizations, such as having a BDFL/CEO. It's important to keep in mind that when the self-management and traditional management can conflict, the later has precedence. Another way to think about this is to see the cell as replacing a middle manager: just like the middle manager, the cell still has to implement the priorities defined by the hierarchy. And a team's manager is also often the best positioned to order his/her team's backlog, and can also have its own pet projects -- but a manager (or here a cell) would still be expected to follow the priorities set by the hierarchy.

## A company as a living organism

Traditional companies are often perceived by their management as machines, where humans are "resources", whose main function is to follow instructions. The top managers pull the lever, the middle managers are the cogs that transmit the order, and the workers execute.

For the type of company we want to be, a different metaphor seems more useful: living organisms. If we take a human for example, it is constituted of many different parts: cells and organs, which collaborate with each other. Each functions independently, and on its own; the brain doesn't need to take every decision for what each individual cell or organ does throughout the day. When needed, it will take over and order a specific reaction, with electric impulses, hormones, etc. But it does so selectively, leaving most of the operations vastly distributed, based on the rules from the DNA code all cells share.

## Cells

To remind ourselves of that approach, our teams are named after one of the building blocks of life: cells.

Each individual contributor belongs to one cell, with cells having up to 12 members. They are the group of people each member knows the best, and works with the closest. A large part of the everyday decisions and responsibilities are delegated to cells and their individual members, to ensure that they can be taken with the full context of the task to complete, and can go from idea to implementation as independently as possible. This isn't independence or anarchy though: cells are all bound by a core set of rules - instead of DNA, we have the current handbook. And the management can still intervene and take different decisions than the cell would have on its own. But this is much more autonomy than a conventional team in a hierchical structure would have, because there is a shared incentive to decentralize the decisions, with all parties trying to reduce the need for management and hierarchy.

Like biological cells, within OpenCraft cells are responsible for the creation of new cells, using a process similar to the mitosis of biological cells: if one grows beyond 12 members, it splits into two cells. Each can then grow again, building upon the experience of members from the parent cell. Cells grow by handling new leads brought in by business development, figuring out which size it needs to have to complete accepted work, reviewing candidates leads and mentoring the accepted ones.

We also have an advantage compared to conventional cells: the ability to rewrite our DNA. You can at any time suggest changes to the set of rules common to all cells by [submitting a PR to the handbook repository](https://gitlab.com/opencraft/documentation/public).

## Cross-cell collaboration

Cells naturally focus a lot of their efforts on their own members, projects and clients, but cells also exist to help each other, like members of a team. Limits on cell size are meant to keep complexity manageable and allow for trust and team work, rather than the maximization of each individual cell's interests. Just like biological cells which together form a larger organism, it's important to ensure that the actions of a cell benefit the larger team, as well as the projects and communities with which we interact.

While all the epics and tasks from a given client are done within a single cell, the one the client owner belongs to, reviewers on individual stories can be assigned across cells. This is useful when there is someone with better expertise or knowledge in a different cell. It is also encouraged to interact across cells, asking questions or offering advices on subjects of interest.

Also, internal projects, such as developing Ocim (our hosting automation software) or attending conferences, are shared between all cells. We are all responsible for those - the way individual contributors of an open source project are responsible for the project together, even if the tasks are split. In a way, the "client" OpenCraft is shared by all cells, although individual epics for that "client" are handled by a single cell at a time.

### Cross-cell planning and time logging

In all cases, time is logged on tickets from the cell owning a specific epic. To allow cells and epic owners to remain in control of their budgets, a contributor from another cell would need the approval (or the request) of a cell member before spending time on a task from that cell. This doesn't need to be too formal - if a member of a cell asks a question or requests the help from someone from another cell, it is sufficient.

To allow proper planning and to allow assignees to follow their tasks from a single board during the sprint, tasks from cell A show on cell B's sprint board when some of the reviewers are from cell B. To allow to clearly differentiate them visually, they are shown in purple on the boards, and a “Hide not in cell” filter is available.

Note that since sprint planning meetings are done per cell, cross-cell assignation of reviewers should be discussed and decided ahead of the sprint meeting as much as possible.

## Next

Next, we will describe how we assign the responsibilities within OpenCraft - using roles.
